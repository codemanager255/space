//
//  HistoricalLaunchesTests.swift
//  SpaceXTests
//
//  Created by Mahesh on 01/28/2020.
//  Copyright © 2020 Mahesh. All rights reserved.
//
//

import XCTest
@testable import SpaceX

class HistoricalLaunchesTests: XCTestCase {
    
    var historicalLaunchesViewController:HistoricalLaunchesViewController!
    var historicalLaunchesViewModel:HistoricalLaunchesViewModel!
    var mockGetHistroicalLunchesService:MockGetHistroicalLunchesService!
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let sb = UIStoryboard(name: "Main", bundle:Bundle.main)
        historicalLaunchesViewController =   sb.instantiateViewController(identifier: "HistoricalLaunchesViewController") as? HistoricalLaunchesViewController
        historicalLaunchesViewController.loadView()
        mockGetHistroicalLunchesService = MockGetHistroicalLunchesService()
        historicalLaunchesViewModel = HistoricalLaunchesViewModel(service: mockGetHistroicalLunchesService)
        historicalLaunchesViewController.viewModel = historicalLaunchesViewModel
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        historicalLaunchesViewModel.spaceXMissions = nil
        mockGetHistroicalLunchesService = nil
        historicalLaunchesViewModel = nil
        historicalLaunchesViewController = nil
    }

    func testGetHistoricalLaunches() {
    
        XCTAssertNil(historicalLaunchesViewModel.spaceXMissions)
        mockGetHistroicalLunchesService.mockDataFileName = "mockSpacex"
        historicalLaunchesViewModel.getHistoricalLaunches()

        XCTAssertEqual(historicalLaunchesViewModel.spaceXMissions?.count,3)
        
    }
    func testCellViewModel() {
        // When spaceXMissions mission is nil
        
        var histroicalLunchesCellViewModel = historicalLaunchesViewModel.cellViewModel(forIndexPath:IndexPath(row:0, section: 0)) as? HistroicalLunchesCellViewModel
    
       XCTAssertNil(histroicalLunchesCellViewModel)
        
        mockGetHistroicalLunchesService.mockDataFileName = "mockSpacex"
        let spaceXMissions = mockGetHistroicalLunchesService.getMockData()
        historicalLaunchesViewModel.spaceXMissions = spaceXMissions
        histroicalLunchesCellViewModel = historicalLaunchesViewModel.cellViewModel(forIndexPath:IndexPath(row:0, section: 0)) as? HistroicalLunchesCellViewModel
        
      XCTAssertEqual(histroicalLunchesCellViewModel?.headerLabelText,"RazakSat")
        
    }
    
    func testNumberOfSections() {
        let sections = historicalLaunchesViewModel.numberOfSections()
        XCTAssertEqual(sections, 1, "Sections not equal to one")
    }
    
    func testRowPerSection() {
        
        // No data scenario
        var rows = historicalLaunchesViewModel.rowsPerSection(forSection:0)
        XCTAssertEqual(rows, 0, "Number of row are not zero though no spacex data ")
        // When there is spaceXMissions data
        mockGetHistroicalLunchesService.mockDataFileName = "mockSpacex"
        let spaceXMissions = mockGetHistroicalLunchesService.getMockData()
        historicalLaunchesViewModel.spaceXMissions = spaceXMissions
        
         rows = historicalLaunchesViewModel.rowsPerSection(forSection:0)
        XCTAssertEqual(rows, 3, "Number of rows are not equal to data  ")
    }
    
    func testViewModelForDetails() {
        // When spaceXMissions mission is nil
        
       var launchesDetailsViewModel = historicalLaunchesViewModel.viewModelForDetailsView(forRow:0) as? LaunchesDetailsViewModel
    
       XCTAssertNil(launchesDetailsViewModel)
        
           // When spaceXMissions mission is not  nil
        mockGetHistroicalLunchesService.mockDataFileName = "mockSpacex"
        let spaceXMissions = mockGetHistroicalLunchesService.getMockData()
        historicalLaunchesViewModel.spaceXMissions = spaceXMissions
        launchesDetailsViewModel = historicalLaunchesViewModel.viewModelForDetailsView(forRow:0) as? LaunchesDetailsViewModel
        
        XCTAssertNotNil(launchesDetailsViewModel)
        
        // Out of bound index scenario
        
        launchesDetailsViewModel = historicalLaunchesViewModel.viewModelForDetailsView(forRow:5) as? LaunchesDetailsViewModel
               
          XCTAssertNil(launchesDetailsViewModel)
        
    }

}

class MockGetHistroicalLunchesService: GetHistroicalLunchesServicable, APIClientDecodable {
    var mockDataFileName = "wrongFileName"
    func gethistoricalLaunches(completion: @escaping CompletionHandler) {
        if let spaceXMissions = self.getMockData() {
          completion(.success(spaceXMissions))
        }else {
            let errorInfo = ErrorInfo(errorCode: .unknown, errorDescription: "unknown error", statusCode: 0)
            completion(.failure(errorInfo))
        }
    }
    func getMockData()-> SpaceXMissions? {
       
        let bundle = Bundle(for:HistoricalLaunchesTests.self)
        if let url =  bundle.url(forResource: mockDataFileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let jsonData = self.decodeServiceResults(SpaceXMissions.self, result: data)
                return jsonData
              } catch {
                print("error:\(error)")
                return nil
            }
        }
        return nil
    }
}
