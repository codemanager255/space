//
//  HistoricalLaunchesViewController.swift
//  SpaceX
//
//  Created by Naem on 01/28/2020.
//  Copyright © 2020 Naem. All rights reserved.
//

import UIKit

class HistoricalLaunchesViewController: UIViewController {
    
    @IBOutlet weak var HLTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    var viewModel:HistoricalLaunchesConfigurable = HistoricalLaunchesViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = viewModel.navigationTitle
        HLTableView.register(BasicTableViewCell.self)
        viewModel.delegate = self
    }
    private func displayLaunchesDetails(forIndexPath indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(identifier: "LunchesDetailsViewController") as? LaunchesDetailsViewController,
            let vm = viewModel.viewModelForDetailsView(forRow: indexPath.row) else { return }
        vc.viewModel = vm
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: -  TableViewDataSource
extension HistoricalLaunchesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.rowsPerSection(forSection: section)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let basicTableViewCell:BasicTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        basicTableViewCell.viewModel = viewModel.cellViewModel(forIndexPath: indexPath)
        return basicTableViewCell
    }
}
// MARK: - TableViewDelegate
extension HistoricalLaunchesViewController: UITableViewDelegate {
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.displayLaunchesDetails(forIndexPath: indexPath)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
}

// MARK: - HistoricalLaunchesDelegate
extension HistoricalLaunchesViewController: HistoricalLaunchesDelegate {
    func reloadDataa() {
        self.HLTableView.reloadData()
    }
    
    func showLoader() {
        DispatchQueue.main.async {
            self.HLTableView.isUserInteractionEnabled = false
            self.activityIndicator?.startAnimating()
        }
    }
    
    func removeLoader() {
        DispatchQueue.main.async {
            self.HLTableView.isUserInteractionEnabled = true
            self.activityIndicator?.stopAnimating()
        }
    }
}
