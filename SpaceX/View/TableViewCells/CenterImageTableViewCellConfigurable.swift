//
//  CenterImageTableViewCellConfigurable.swift
//  SpaceX
//
//  Created by Naem on 01/28/2020.
//  Copyright © 2020 Naem. All rights reserved.
//

import Foundation
import UIKit

public protocol CenterImageTableViewCellConfigurable: CellViewModelConfigurable {
    var defaultImage: UIImage? { get }
    var imageUrl: String?       { get }
}
