//
//  BasicTableViewCell.swift
//  SpaceX
//
//  Created by Naem on 01/28/2020.
//  Copyright © 2020 Naem. All rights reserved.
//

import UIKit
import Kingfisher

class BasicTableViewCell: UITableViewCell, ReusableView, NibLoadableView{

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var stackLabel1: UILabel!
    @IBOutlet weak var stackLabel2: UILabel!
    @IBOutlet weak var stackLabel3: UILabel!
    @IBOutlet weak var cellImageView: UIImageView!
    var urlString: String = ""
    var defaultImage: UIImage?
    
    var viewModel: CellViewModelConfigurable! {
        didSet {
            guard let vm = viewModel as? BasicTableViewCellConfigurable else { return }
            self.headerLabel.text = vm.headerLabelText ?? ""
            self.stackLabel1.text = vm.stackLabel1Text ?? ""
            self.stackLabel2.text = vm.stackLabel2Text ?? ""
            self.stackLabel3.text = vm.stackLabel3Text ?? ""
            self.urlString = vm.imageUrl ?? ""
            self.defaultImage =  UIImage(named:vm.cellDefaultName)
            self.setImage()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func setImage() {
        guard let url = URL(string: urlString) else {
            cellImageView.image = defaultImage
            return
        }
        DispatchQueue.main.async {
            self.cellImageView.kf.indicatorType = .activity
            self.cellImageView?.kf.setImage(with: url, placeholder: self.defaultImage)
        }
       
    }
}
