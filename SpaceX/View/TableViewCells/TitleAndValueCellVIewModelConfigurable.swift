//
//  TitleAndValueCellVIewModelConfigurable.swift
//  SpaceX
//
//  Created by Naem on 01/28/2020.
//  Copyright © 2020 Naem. All rights reserved.
//

import Foundation

public protocol TitleAndValueCellVIewModelConfigurable: CellViewModelConfigurable {
    var titleText: String? { get }
    var valueText: String? { get }
}
