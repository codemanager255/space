//
//  TitleAndValueTableViewCell.swift
//  SpaceX
//
//  Created by Adel Morgan on 11/10/19.
//  Copyright © 2019 Adel Morgan. All rights reserved.
//

import UIKit

class TitleAndValueTableViewCell: UITableViewCell , ReusableView, NibLoadableView{

    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    var viewModel: CellViewModelConfigurable! {
        didSet {
            guard let vm = viewModel as? TitleAndValueCellVIewModelConfigurable else {
                return
            }
            titleLabel.text = vm.titleText ?? ""
            valueLabel.text = vm.valueText ?? ""
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        DispatchQueue.main.async {
            self.borderView.layer.borderColor = UIColor.black.cgColor
            self.borderView.layer.borderWidth = 2.0
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public static var cellReuseId: String {
        return "TitleAndValueTableViewCell"
    }
    
}
