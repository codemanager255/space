//
//  CenterImageTableViewCell.swift
//  SpaceX
//
//  Created by Naem on 01/28/2020.
//  Copyright © 2020 Naem. All rights reserved.
//

import UIKit
import Kingfisher

class CenterImageTableViewCell: UITableViewCell, ReusableView, NibLoadableView {

    @IBOutlet weak var centerImageView: UIImageView!
    var defaultImage: UIImage = UIImage()
    var imageUrl: String = ""
    
    var viewModel: CellViewModelConfigurable! {
        didSet {
            guard let vm = viewModel as? CenterImageTableViewCellConfigurable else {
                return
            }
            self.defaultImage = vm.defaultImage ?? UIImage()
            self.imageUrl = vm.imageUrl ?? ""
            self.setImage()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setImage() {
        guard let url = URL(string: imageUrl) else {
            centerImageView.image = defaultImage
            return
        }
        DispatchQueue.main.async {
            self.centerImageView.kf.indicatorType = .activity
            self.centerImageView?.kf.setImage(with: url, placeholder: self.defaultImage)
        }
        self.layoutSubviews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public static var cellReuseId: String {
        return "CenterImageTableViewCell"
    }
}
