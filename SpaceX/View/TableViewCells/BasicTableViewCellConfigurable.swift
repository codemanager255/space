//
//  BasicTableViewCellConfigurable.swift
//  SpaceX
//
//  Created by Naem on 01/28/2020.
//  Copyright © 2020 Naem. All rights reserved.
//


import UIKit

public protocol BasicTableViewCellConfigurable: CellViewModelConfigurable {
    var headerLabelText: String? { get }
    var stackLabel1Text: String? { get }
    var stackLabel2Text: String? { get }
    var stackLabel3Text: String? { get }
    var cellDefaultName: String  { get }
    var imageUrl: String? { get }
}
