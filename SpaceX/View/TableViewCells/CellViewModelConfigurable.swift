//
//  CellViewModelConfigurable.swift
//  SpaceX
//
//  Created by Naem on 01/28/2020.
//  Copyright © 2020 Naem. All rights reserved.
//

import Foundation

public protocol CellViewModelConfigurable {
    
}

public struct CellViewModelPlaceHolder: CellViewModelConfigurable {
    
}
