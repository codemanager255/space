//
//  SpaceXConstants.swift
//  SpaceX
//
//  Created by Naem on 01/28/2020.
//  Copyright © 2020 Naem. All rights reserved.
//

import UIKit

public enum ErrorCode: String {
    case badUrl = "URL_Error_01"
    case badRequest = "Request_Error_01"
    case errorResponse = "Response_Error_01"
    case unknown = "Unkown_Error_01"
    case parsingFailed = "Data_Parsing_error"
}

public struct SpaceXConstatns {
    public static let navBarColor = UIColor(red: 0.0, green: 0.0, blue: 0.5, alpha: 1.0)
}
