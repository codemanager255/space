//
//  HistoricalLaunchesViewModel.swift
//  SpaceX
//
//  Created by Mahesh on 01/28/2020.
//  Copyright © 2020 Mahesh. All rights reserved.
//

import Foundation

final class HistoricalLaunchesViewModel: HistoricalLaunchesConfigurable {
    
    var service:GetHistroicalLunchesServicable!

    var spaceXMissions: SpaceXMissions? {
        didSet {
          self.delegate?.reloadDataa()
        }
    }
    public weak var delegate: HistoricalLaunchesDelegate? {
        didSet {
            self.getHistoricalLaunches()
        }
    }
    public var navigationTitle: String {
        return "Historical Launches"
    }
    init(service:GetHistroicalLunchesServicable = GetHistroicalLunchesService()) {
        self.service = service
    }
    public func numberOfSections() -> Int {
        return 1
    }
    
    public func cellViewModel(forIndexPath indexPath: IndexPath) -> CellViewModelConfigurable {
        guard let _spaceXMissions = spaceXMissions, indexPath.row < _spaceXMissions.count else {
            return CellViewModelPlaceHolder()
        }
        return HistroicalLunchesCellViewModel(mission: _spaceXMissions[indexPath.row])
    }
    
    public func rowsPerSection(forSection section: Int) -> Int {
        return spaceXMissions?.count ?? 0
    }
    
    public func viewModelForDetailsView(forRow row: Int) -> LaunchesDetailsViewModelConfigurable? {
        guard  let _spaceXMissions = spaceXMissions, row < _spaceXMissions.count else { return nil }
        return LaunchesDetailsViewModel(mission: _spaceXMissions[row])
    }
    
    func getHistoricalLaunches() {
        self.delegate?.showLoader()
        GetHistroicalLunchesService().gethistoricalLaunches{ [weak self] (result) in
            switch result {
            case .success(let spaceXMissions):
                self?.delegate?.removeLoader()
                if let missionsData = spaceXMissions as? SpaceXMissions {
                    self?.spaceXMissions = missionsData
                }
            case .failure(let error):
                print(error.localizedDescription)
                self?.delegate?.removeLoader()
            }
        }
    }
}
