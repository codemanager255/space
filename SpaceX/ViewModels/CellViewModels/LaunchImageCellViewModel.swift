//
//  LaunchImageCellViewModel.swift
//  SpaceX
//
//  Created by Mahesh on 01/28/2020.
//  Copyright © 2020 Mahesh. All rights reserved.
//

import Foundation
import UIKit

public struct LaunchImageCellViewModel: CenterImageTableViewCellConfigurable  {
    var spaceXMission: SpaceXMission?
    
    init (mission: SpaceXMission?) {
        self.spaceXMission = mission
    }
    
    public var defaultImage: UIImage? {
        return UIImage(named: "spaceXLogo")
    }
    
    public var imageUrl: String? {
        return ""//spaceXMission?.links.missionPatch
    }
}
