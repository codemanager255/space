//
//  LaunchDetailsCellViewModel.swift
//  SpaceX
//
//  Created by Mahesh on 01/28/2020.
//  Copyright © 2020 Mahesh. All rights reserved.
//

import Foundation

public struct LaunchDetailsCellViewMode: TitleAndValueCellVIewModelConfigurable {
    public var titleText: String?
    public var valueText: String?

    init(title: String?, value: String?) {
        self.titleText = title
        self.valueText = value
    }
}
