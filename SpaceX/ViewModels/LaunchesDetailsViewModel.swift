//
//  LaunchesDetailsViewModel.swift
//  SpaceX
//
//  Created by Mahesh on 01/28/2020.
//  Copyright © 2020 Mahesh. All rights reserved.
//

import Foundation

enum LaunchDetailsSections: Int {
    case launchImage, launchData
    static var count: Int { return LaunchDetailsSections.launchData.rawValue + 1 }
}

public protocol LaunchesDetailsViewModelDelegate: class {
    func reloadData()
}

public struct LaunchesDetailsViewModel: LaunchesDetailsViewModelConfigurable {
    
    var spaceXMission: SpaceXMission?
    var missionDictionary = [String: String]()
    var keys = [String]()
    
    init (mission: SpaceXMission?) {
        self.spaceXMission = mission
        
        //TODO: Add any key value to the missionDictionary
        // you can add any fileds to display it to the user in details view controller
        missionDictionary["1. Mission Name:"] = spaceXMission?.missionName ?? ""
        missionDictionary["2. Mission ID:"] = spaceXMission?.missionID.first ?? ""
        missionDictionary["3. Launch Year:"] = spaceXMission?.launchYear ?? ""
        missionDictionary["4. Launch Window:"] = String(spaceXMission?.launchWindow ?? 0)
        missionDictionary["5. Is Tentative:"] = String(spaceXMission?.isTentative ?? false)
     //   missionDictionary["6. Ship Name:"] = spaceXMission?.ships.first ?? ""
        keys = Array(missionDictionary.keys).sorted{$0 < $1}
    }
    
    weak public var delegate: LaunchesDetailsViewModelDelegate?
    
    public func numberOfSections() -> Int {
        return LaunchDetailsSections.count
    }
    
    
    public func cellViewModel(forIndexPath indexPath: IndexPath) -> CellViewModelConfigurable {
        
        guard let section = LaunchDetailsSections(rawValue: indexPath.section) else {
            return CellViewModelPlaceHolder()
        }
        switch section {
        case .launchImage:
            return LaunchImageCellViewModel(mission: spaceXMission)
        case .launchData:
            guard  indexPath.row < keys.count else { return CellViewModelPlaceHolder() }
            let title = keys[indexPath.row]
            let value = missionDictionary[title]
            return LaunchDetailsCellViewMode(title: title, value: value)
        }
    }
    
    public func rowsPerSection(forSection section: Int) -> Int {
        guard let section = LaunchDetailsSections(rawValue: section) else {
            return 0
        }
        switch section {
        case .launchImage:
            return 1
        case .launchData:
            return missionDictionary.count
        }        
    }
}
