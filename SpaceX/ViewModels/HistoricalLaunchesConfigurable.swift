//
//  HistoricalLaunchesConfigurable.swift
//  SpaceX
//
//  Created by Mahesh on 01/28/2020.
//  Copyright © 2020 Mahesh. All rights reserved.
//

import Foundation

public protocol HistoricalLaunchesDelegate: class {
    func reloadDataa()
    func showLoader()
    func removeLoader()
}

public protocol HistoricalLaunchesConfigurable: TableViewDelegateAndDataSourceConfigurable {
    var navigationTitle: String { get }
    var delegate: HistoricalLaunchesDelegate? { get set }
    func viewModelForDetailsView(forRow row: Int) -> LaunchesDetailsViewModelConfigurable?
}
