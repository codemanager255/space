//
//  APIClientServiceBase.swift
//  SpaceX
//
////  Created by Mahesh on 01/28/2020.
//  Copyright © 2020 Mahesh. All rights reserved.
//
import Foundation
import Alamofire

protocol APIClientServicable {
    var method: HTTPMethod {get set}
    var path: String {get set}
    var parameters: Parameters? {get set}
    func sendRequest(completionHandler:  @escaping CompletionHandler)
}
protocol APIClientDecodable {
    func decodeServiceResults <T: Codable> (_ type: T.Type, result:Data?) -> T?
}
extension APIClientDecodable {
    /// function to map any response to Codable type object
    /// - Parameters:
    ///   - type: Generic type of codable object
    ///   - result: using ServiceCallResult block
    public func decodeServiceResults <T: Codable> (_ type: T.Type, result:Data?) -> T? {
        let newJSONDecoder = JSONDecoder()
        guard let dataJSON = result else { return nil }
        do {
            return  try newJSONDecoder.decode(T.self, from: dataJSON)
        }catch {
            print(error.localizedDescription)
            return nil
        }
    }
}
extension APIClientServicable {
    
    /// Method to send all request without paramters using AF framework
     /// - Parameters:
     ///   - success: success closure
     ///   - failure: failure closure
     public func sendRequest(completionHandler:  @escaping CompletionHandler) {
         
         guard let url = URL(string: path) else {
             let errorInfo = ErrorInfo(errorCode: .badUrl, errorDescription: "Bad or missing url", statusCode: 0)
             completionHandler(.failure(errorInfo))
             return
         }
         guard let urlRequest = try? URLRequest(url: url, method: self.method, headers: nil) else {
             let errorInfo = ErrorInfo(errorCode: .badRequest, errorDescription: "Bad or missing request data", statusCode: 0)
              completionHandler(.failure(errorInfo))
             return
         }
         AF.request(urlRequest).response { (response) in
             guard response.error == nil else {
                 let errorInfo = ErrorInfo(errorCode: .errorResponse, errorDescription: response.response.debugDescription, statusCode: response.response?.statusCode ?? 400)
                  completionHandler(.failure(errorInfo))
                 return
             }
             if let data  = response.data {
                 completionHandler(.success(data))
             }
         }
     }
}
