//
//  APIClientConfigurable.swift
//  SpaceX
//
//  Created by Mahesh on 01/28/2020.
//  Copyright © 2020 Mahesh. All rights reserved.
//

import Foundation
import Alamofire


typealias CompletionHandler = (_ result: Result<Any, ErrorInfo>) -> Void

struct ErrorInfo: Error {
     var errorCode: ErrorCode = .unknown
     var errorDescription: String?
     var statusCode: Int = 400
     var parsingFailed:ErrorCode = .parsingFailed
}

