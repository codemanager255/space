//
//  GetHistroicalLunchesService.swift
//  SpaceX
//
//  Created by Mahesh on 01/28/2020.
//  Copyright © 2020 Mahesh. All rights reserved.
//

import Foundation
import Alamofire
//import SwiftyJSON
protocol GetHistroicalLunchesServicable {
    func gethistoricalLaunches(completion:@escaping CompletionHandler)
}

final class GetHistroicalLunchesService: APIClientServicable, APIClientDecodable, GetHistroicalLunchesServicable {
    var method: HTTPMethod
    var path: String
    var parameters: Parameters?
     init() {
        method = .get
        path = "https://api.spacexdata.com/v3/launches/past"
        parameters = nil
    }
    func gethistoricalLaunches(completion:@escaping CompletionHandler) {
        self.sendRequest{ (result) in
            switch result {
            case .success(let data):
                if let _data = data as? Data {
                    if  let serilizedObject = self.decodeServiceResults(SpaceXMissions.self, result:_data) {
                        completion(.success(serilizedObject))
                    }else {
                         let errorInfo = ErrorInfo(errorCode: .parsingFailed, errorDescription: "Data parsing failed", statusCode: 0)
                        completion(.failure(errorInfo))
                    }
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
